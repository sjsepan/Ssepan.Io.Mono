# readme.md - README for Ssepan.Io.Mono v6.0

## About

Common library of i/o functions for C# Mono applications; requires ssepan.utility.mono

### Purpose

To encapsulate common i/o functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Setup

### History

6.0:
~initial Mono release

Steve Sepan
ssepanus@yahoo.com
5/30/2022

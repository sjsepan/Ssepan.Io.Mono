﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using Ssepan.Utility.Mono;

namespace Ssepan.Io.Mono
{
    public class Folder
    {
        /// <summary>
        /// Copy file from source path to destination path.
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="destinationPath"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool CopyFile
        (
            string sourcePath,
            string destinationPath,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);

            try
            {
                //copy source to destination
                //Note: could use Files.Copy(), but this provides more options
                FileSystem.CopyFile
                (
                    sourcePath, //inputpath + docfolder(s)
                    destinationPath, //outputpath + "Images"
                    UIOption.AllDialogs,
                    UICancelOption.ThrowException
                );

                returnValue = true;
            }
            catch (OperationCanceledException ex)
            {
                errorMessage = ex.Message;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //cancelled
                throw new Exception(string.Format("Copy cancelled: {0}", ex.Message));
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

            }

            return returnValue;
        }
        /// <summary>
        /// Delete and re-create folder
        /// </summary>
        /// <param name="outputPath"></param>
        /// <param name="folder"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static bool ReCreateFolder
        (
            string outputPath,
            string folder,
            ref string errorMessage
        )
        {
            bool returnValue = default(bool);
            string destinationPath = Path.Combine(outputPath, folder);

            try
            {
                //remove pre-existing documentsFolder
                if (Directory.Exists(destinationPath))
                {
                    //Directory.Delete(destinationPath, true);
                    //deal with locking/timing issues
                    DeleteFolderWithWait(destinationPath, 500);
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;

                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw new Exception(string.Format(" Unable to delete '{0}' \n{1}", destinationPath, ex.Message));
            }

            try
            {
                //create docsfolder at destination
                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format(" Unable to create '{0}' \n{1}", destinationPath, ex.Message));
            }

            returnValue = true;

            return returnValue;
        }


        /// <summary>
        /// Perform Directory.Delete() with a wait time to allow the system to catch up.
        /// </summary>
        /// <param name="folderPath"></param>
        /// <param name="waitMilliSeconds"></param>
        /// <param name="reCreate"></param>
        public static void DeleteFolderWithWait(string folderPath, int waitMilliSeconds)
        {
            try
            {
                //check for folder and delete if present
                if (Directory.Exists(folderPath))
                {
                    try
                    {
                        Directory.Delete(folderPath, true);
                    }
                    catch (IOException)
                    {
                        //handle locks by explorer
                        /*await*/ Task.Delay(3000);
                        Directory.Delete(folderPath, true);
                    }
                }

                //allow system to catch up before checking for folder *again*
                /*await*/ Task.Delay(waitMilliSeconds);
                //Application.DoEvents();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                        
            }
        }
    }
}
